# import non
#
# i1 = non.Entity(1)
# i1.add_field("name","university Exemple")
# i1.add_field("domain","exemple.tld")
# print(i1.fields)
#
# i2 = non.Entity(2,i1)
# i2.add_field("cours","maths")
# print(i2.fields)

from non import *
import textx

from textx import metamodel_from_file

con_meta = metamodel_from_file('rules.tx')

non = con_meta.model_from_file('exemple.ent')

entities = []
for e in non.entities:
    entity = Entity(e.name,con_meta, e.super )
    for FieldObject in e.fields:
        entity.add_field(FieldObject.field_name, FieldObject.type)
    print(entity.id, end=" ")
    print(entity.fields)
    entities.append(entity)

print()
robert = entities[3]
alice = entities[2]

print(robert.get_field("login"))
print(robert.get_field("mail"))
print(robert.get_field("name"))
print()
print(alice.get_field("login"))
print(alice.get_field("mail"))
print(alice.get_field("name"))
print(alice.get_field("egirl"))
print()
print(alice.serialization_direct(),end = "\n")
print(alice.serialize_json(),end = "\n")


    # print(FieldObject.field_name)
    # for field in FieldObject.type:
    #     # print(field)

    # if textx.textx_isinstance(field,con_meta["Ref"]):
    #     print(field)
    #
    #         if isinstance(str,field):
    #
    #         elif isinstance(field.type, "rules.Ref") :
    #             value.append(field.type.name)
    #
    #         elif isinstance(field.type, "rules.Identifier") :
    #             value.append(field.type.name)
    #         else:
    #             value.append(field.type)
    # entity.add_field(FieldObject.name, value)


### serialization dans un fichier ###
#serialization_alice = alice.serialization_direct()
#f = open("serialization_test.ent","a")
#for element in serialization_alice :
#    f.write(element)
#f.close()
