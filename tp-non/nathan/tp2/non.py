import textx
import json


class Entity:

    def __init__(self, number, model, entity=None):
        self.fields = dict()
        if entity is not None:
            for FieldObject in entity.fields:
                self.fields[FieldObject.field_name] = FieldObject.type

        # self.fields = dict() if entity is None else entity.fields
        self.name = number
        self.id = number
        self.model = model
        self.entity = entity

    def add_field(self, name, value):
        self.fields.update({name: value})

    def update_field(self, name, value):
        self.fields.update({name: value})

    def get_field_non_evaluate(self, name):
        res = []
        for elem in self.fields.get(name):

            if textx.textx_isinstance(elem, self.model["Ref"]):

                res = + elem.ref_name
            else:
                res += elem
        return res

    def exist_field(self, name):
        return name in self.fields.keys()

    def get_field(self, name):
        res = ""

        for elem in self.fields.get(name):
            if textx.textx_isinstance(elem, self.model["Ref"]):
                if self.fields.get(elem.ref_name)is not None and self.fields.get(elem.ref_name)[0] == '@':
                    res += self.id
                else:
                    res += "notFound" if self.fields.get(elem.ref_name) is None else self.fields.get(elem.ref_name)[0]
            elif elem == "@":
                res += self.id
            else:
                res += elem

        return res

    def serialization_direct(self):
        seria = []
        string = self.id + ': '
        if self.entity is not None :
            string += self.entity.name
        seria.append(string)
        for key, value in self.fields.items() :
            string = ''
            for element in value :
                string += element.ref_name if textx.textx_isinstance(element, self.model["Ref"]) else element
            seria.append('.' + key + ' ' + string)

        res = ""
        for i in seria:
            res += i+"\n"
        return res


    def serialize_json(self):

        file = {}
        string = self.id + ': '

        for key, value in self.fields.items():
            string = ''
            for element in value:
                string += element.ref_name if textx.textx_isinstance(element, self.model["Ref"]) else element

            file[key] = string


        parent=dict()
        parent[self.id + ': '+self.entity.name]=file
        json.dumps(parent)
        return parent
