
import abc
from textx import metamodel_from_file

class NonFieldException(Exception):
    pass

class Non:
    def __init__(self):
        self.objects = {}
    
    def set(self, object_name, obj):
        self.objects[object_name] = obj
    def get(self, object_name):
        return self.objects[object_name]

    def __getitem__(self, item):
        return self.get(item)

    def serialize(self):
        return "\n".join(v.serialize() for v in self.objects.values())



class NonObject:
    def __init__(self, non, name, inherit = None):
        self.non = non
        self.inherit = inherit
        self.name = name
        self.fields = {}
    def set(self, field_name, field_value):
        self.fields[field_name] = field_value

    def get(self,field_name):
        if field_name in self.fields:
            return self.fields[field_name].get()
        elif self.inherit != None and self.inherit != "":
            return self.non.get(self.inherit).get(field_name)
        else:
            raise NonFieldException()

    def __getitem__(self, item):
        return self.get(item)

    def serialize(self):
        res = self.name + ": " + (self.inherit if self.inherit else "") + "\n"
        for k,v in self.fields.items():
            res+="."+k+" "+v.serialize()+"\n"
        return res


class NonAbstractFieldValue:
    @abc.abstractmethod
    def get(self):
        return

    @abc.abstractmethod
    def serialize(self):
        return

class NonStringFieldValue(NonAbstractFieldValue):
    def __init__(self,value):
        self.value = value

    def get(self):
        return self.value

    def serialize(self):
        return "'" + self.value + "'"

class NonConcatFieldValue(NonAbstractFieldValue):
    def __init__(self, left_field, right_field):
        self.left_field = left_field
        self.right_field = right_field

    def get(self):
        return self.left_field.get() + self.right_field.get() 
    
    def serialize(self):
        return self.left_field.serialize() + " " + self.right_field.serialize()

class NonExternalRefFieldValue(NonAbstractFieldValue):
    def __init__(self, non, non_object_name, fied_name):
        self.non = non
        self.non_object_name = non_object_name
        self.field_name = fied_name

    def get(self):
        return self.non.get(self.non_object_name).get(self.field_name)
    
    def serialize(self):
        return self.non_object_name + "." + self.field_name

class NonLocalRefFieldValue(NonAbstractFieldValue):
    def __init__(self, non_owner_object, fied_name):
        self.non_owner_object = non_owner_object
        self.field_name = fied_name

    def get(self):
        return self.non_owner_object.get(self.field_name)

    def serialize(self):
        return '.' + self.field_name
        
class NonObjetIdFieldValue(NonAbstractFieldValue):
    def __init__(self, non_object):
        self.non_object = non_object
    def get(self):
        return self.non_object.name

    def serialize(self):
        return "@"


non_mm = metamodel_from_file('non.tx')
non_model = non_mm.model_from_file('non.ent')
non = Non()

for obj in non_model.objects:
    non_obj = NonObject(non,obj.id,obj.super)
    for f in obj.fields:
        fieldValue = None
        #print(f.fieldValue) 
        if f.fieldValue.__class__.__name__ == "Str":
            fieldValue = NonStringFieldValue(f.fieldValue.value)
        if f.fieldValue.__class__.__name__ == "LocRef":
            fieldValue = NonLocalRefFieldValue(non_obj,f.fieldValue.ref)
        if f.fieldValue.__class__.__name__ == "str":
            fieldValue = NonObjetIdFieldValue(non_obj)
        if f.fieldValue.__class__.__name__ == "ExtRef":
            fieldValue = NonExternalRefFieldValue(non,f.fieldValue.objRef,f.fieldValue.ref)
        non_obj.set(f.field_name,fieldValue)
    non.set(non_obj.name,non_obj)


print(non.get("univ").get("domain"))
print(non.get("univ").get("domain"))
print(non.get("bob").get("login"))
print(non["univ"]["domain"])
print(non["alice"]["dom2"])

print(non.serialize())