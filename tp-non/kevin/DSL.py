
import sys

class NonFieldException(Exception):
    pass



class NonObject:
    def __init__(self, name, inherit = None):
        self.id = name
        self.attributes = {}

    def set(self, attribute_name, attribute_value):
        self.attributes[attribute_name] = attribute_value
    
    def nonId(self):
        return self.id

    
    def get(self, attribute_name):
        if (attribute_name in self.attributes):
            return self.attributes[attribute_name]
        else:
            raise NonFieldException("NonFieldException")



test = NonObject("coucou")

test.set("choufleur", "4")
print("Attribut choufleur : " + test.get("choufleur"))
#rint(test.get("aga"))  #Produce NonFieldException
test.nonId()
print("Identifiant : " + test.nonId())