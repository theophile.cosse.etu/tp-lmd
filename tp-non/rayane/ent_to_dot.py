from textx import metamodel_from_file
from textx.export import metamodel_export

entity_mm = metamodel_from_file('entity.tx')

metamodel_export(entity_mm, 'entity.dot')

from textx.export import model_export

non_model = entity_mm.model_from_file('non.ent')

model_export(non_model, 'non.dot')