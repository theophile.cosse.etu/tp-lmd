public class Standing implements HeroState{
@Override
public HeroState down(){
return new Crouching();}

@Override
public HeroState release(){
return this;}

@Override
public HeroState up(){
return new Jumping();}

@Override
public HeroState auto(){
return this;}

}