public interface HeroState {
public HeroState down ();
public HeroState release ();
public HeroState up ();
public HeroState auto();
}