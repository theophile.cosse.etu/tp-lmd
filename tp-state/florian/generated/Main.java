

public class Main  { 

    public static void main (String[] args) {
        Crouching c = new Crouching();
        Standing s = new Standing();
        Diving d = new Diving();
        Jumping j = new Jumping();

        if (d.auto() instanceof Standing) {
            System.out.println("Passe de Diving a Standing");
        }

        if (s.auto() instanceof Standing) {
            System.out.println("Passe automatiquement de Standing a...... toujours Standing");
        }

        if(s.up() instanceof Jumping) { 
            System.out.println("Passe de Standing a Jumping via la commande UP !");
        }
    }
}