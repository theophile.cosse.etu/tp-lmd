public class Crouching implements HeroState{
@Override
public HeroState down(){
return this;}

@Override
public HeroState release(){
return new Standing();}

@Override
public HeroState up(){
return this;}

@Override
public HeroState auto(){
return this;}

}