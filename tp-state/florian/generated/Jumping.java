public class Jumping implements HeroState{
@Override
public HeroState down(){
return new Diving();}

@Override
public HeroState release(){
return this;}

@Override
public HeroState up(){
return this;}

@Override
public HeroState auto(){
return new Standing();}

}