# QUESTION 1

class StatePattern :
    def __init__ (self, name):
        self.name = name
        self.classes = set()
        self.fonctions = set()
        self.transitions = set()

    def addClasse(self, name) :
        self.classes.add(name)

    def addFonction(self,name) :
        self.fonctions.add(name)

    def addTransitionWithState(self, origine, destination, fonction) :
        assert origine in self.classes and destination in self.classes, "La classe d'origine ou la classe de destination n'existe pas" 
        assert fonction in self.fonctions, "La fonction de transition indiquée n'existe pas"
        self.transitions.add((origine, destination, fonction))

    def addTransition(self, origine, destination) :
        assert origine in self.classes and destination in self.classes, "La classe d'origine ou la classe de destination n'existe pas" 
        self.transitions.add((origine, destination))


    def generateJavaCode(self) :
        # Création de l'interface
        fInt = open("./generated/" + self.name + ".java", 'w')
        fInt.write("public interface " + self.name + " {\n")
        for f in self.fonctions :
            fInt.write("public " + self.name + " " + f + " ();\n")
        fInt.write("public " + self.name + " auto();\n")
        fInt.write("}")
        fInt.close()

        for c in self.classes :
            fClass = open("./generated/" + c + ".java", 'w')
            fClass.write("public class " + c + " implements " + self.name + "{\n")

            # Dictionnaire des destination des fonctions
            dic = {}
            for f in self.fonctions :
                dic[f] = "this"
            dic["auto"] = "this"

            # Pour chaque transition avec origine = c : on redefinit la fonction adequate
            for t in self.transitions :
                if (t[0] == c) :
                    if len(t) == 3 :
                        dic[t[2]] = 'new ' + t[1] + '()'
                    else : dic["auto"] = 'new ' + t[1] + '()'

            # On génère le code des fonctions
            for d in dic.keys() :
                fClass.write("@Override\npublic " + self.name + " "  + d +"(){\nreturn " + dic[d] + ";}\n\n")
            fClass.write("}")

            fClass.close()

    def generateGraphz(self) :
        fgz = open("./generated/" + self.name + ".dot", 'w')
        fgz.write("digraph " + self.name + " {\n")

        for t in self.transitions :
            if len(t) == 3 :
                fgz.write(t[0] + " -> " + t[1] + '[label = "' + t[2] + '"];\n')
            else :
                fgz.write(t[0] + " -> " + t[1] + ";\n")
        fgz.write("}")


def import_graphviz(file) :
    f = open(file,'r')
    fLine = f.readline()
    name = fLine.split(' ')[1]
    statePattern = StatePattern(name)
    for l in f :

        # On ignore la derniere ligne de facon degueulasse
        if(len(l) < 4) :
            break

        #print(l[:-1] + ':')
        # ORIGINE
        origine = l.split('->')[0].strip()

        if(origine[0] == '{'):
            listOfOrigins = origine[1:-1].split(',')
            for i in range(0,len(listOfOrigins)):
                listOfOrigins[i] = listOfOrigins[i].strip()
            origine = listOfOrigins

        else :
            origine = [origine.strip()]

        hasFunction = False

        # DESTINATION
        droite = l.split('->')[1]
        if 'label' in droite :
            hasFunction = True
            
            destination = droite.split('[')[0].strip()

            # FONCTION
            label = droite.split('[')[1]
            fonctionGuillemets = label.split('=')[1][:-3]
            fonction = fonctionGuillemets.strip()[1:-1].strip()
                        
        else :
            destination = droite[:-2].strip()

        for o in origine : 
##            print("Translated to :\nOrigine : " + o + "\nDestination : " + destination + "\n")
##            if hasFunction :
##                print("Function : " + fonction + "\n")

            # AJOUTS EVENTUELS AU STATEPATTERN
            if not o in statePattern.classes :
                statePattern.addClasse(o)
            if not destination in statePattern.classes :
                statePattern.addClasse(destination)
            if hasFunction :
                if not fonction in statePattern.fonctions:
                    statePattern.addFonction(fonction)

            # CREATION DE LA TRANSITION
            if hasFunction :
                statePattern.addTransitionWithState(o,destination,fonction)
            else :
                statePattern.addTransition(o,destination)

    return statePattern
                


                    

# QUESTION 2

herostate = StatePattern("HeroState")
herostate.addClasse("Standing")
herostate.addClasse("Jumping")
herostate.addClasse("Crouching")
herostate.addClasse("Diving")
herostate.addFonction("up")
herostate.addFonction("down")
herostate.addFonction("release")
herostate.addTransitionWithState("Standing","Jumping","up")
herostate.addTransitionWithState("Standing","Crouching","down")
herostate.addTransitionWithState("Jumping","Diving","down")
herostate.addTransitionWithState("Crouching","Standing","release")
herostate.addTransition("Jumping","Standing")
herostate.addTransition("Diving","Standing")
herostate.generateJavaCode()
herostate.generateGraphz()
print("Affichage d'une conversion du fichier source.txt")
s = import_graphviz("./source.dot")
print("Classes : " + str(s.classes) + "\n")
print("Fonctions : " + str(s.fonctions) + "\n")
print("Transitions : " + str(s.transitions) + "\n")

# QUESTION 3
# seul des changements d'états sont supportés par ce modèle

# QUESTION 4

