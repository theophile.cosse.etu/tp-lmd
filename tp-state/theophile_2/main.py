from textx import metamodel_from_file
from mako.template import Template
import subprocess

class StatePattern:
    def __init__(self,name):
        self.name = name
        self.states = {}
        self.transitions_names = []

class State:
    def __init__(self,name):
        self.name = name
        self.transitions = []

class Transition:
    def __init__(self,target,name="defaultT"):
        self.name = name
        self.target = target


def create_state_pattern(mm):
    sp = StatePattern(mm.name)
    create_states(sp,mm)
    create_transitions(sp,mm)
    return sp

def create_states(sp,mm):
    states_set = []
    for ts in mm.tf:
        if hasattr(ts,'source'):
            if ts.source not in states_set:
                states_set.append(ts.source)
                sp.states[ts.source] = State(ts.source)
        else:
            for sub_ts in ts.sources:
                if sub_ts not in states_set:
                    states_set.append(sub_ts)
                    sp.states[sub_ts] = State(sub_ts)

def create_transitions(sp,mm):
    for ts in mm.tf:
        if hasattr(ts,'label') and ts.label not in sp.transitions_names:
            sp.transitions_names.append(ts.label)
        if hasattr(ts,'source'):
            t = Transition(sp.states[ts.target])
            sp.states[ts.source].transitions.append(t)
            if hasattr(ts,'label'):
                t.name = ts.label
                if ts.label not in sp.transitions_names:
                    sp.transitions_names.append(ts.label)
        else:
            for sub_ts in ts.sources:
                t = Transition(sp.states[ts.target])
                if hasattr(ts,'label'):
                    t.name = ts.label
                sp.states[sub_ts].transitions.append(t)


dot_parser = metamodel_from_file('dot_parser.tx')
dot_mm = dot_parser.model_from_file('door.dot')
sp = create_state_pattern(dot_mm)

template = Template(filename='template')
f = open("./gen/source/" + sp.name + ".java", 'w')
f.write(template.render(sp=sp))
f.close()

subprocess.run(["javac", "-d","./gen/classes/","./gen/source/"+sp.name+".java"])

#todo serialize