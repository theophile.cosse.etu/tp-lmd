

interface State {
    default void up(StateContext context) {}
    default void down(StateContext context) {}
    default void release(StateContext context) {}

}

class Crouching implements State {
    @Override
    public void release(StateContext context) {
        System.out.println("Crouching state");
        context.setState(new Standing());
    }
}


class HeroContext {
    private State state;
    
    public StateContext() {
        state = new Crouching();
    }

    /**
     * Set the current state.
     * Normally only called by classes implementing the State interface.
     * @param newState the new state of this context
     */
    void setState(State newState) {
        state = newState;
    }
    

}