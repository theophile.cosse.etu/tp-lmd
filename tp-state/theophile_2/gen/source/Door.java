
interface State {
       default void close (DoorContext context) {}
       default void open (DoorContext context) {}
       default void unlock (DoorContext context) {}
       default void lock (DoorContext context) {}
       default void defaultT (DoorContext context) {}
}


class Opened implements State {
    @Override
    public void close (DoorContext door) {
        System.out.println("Opened state");
        door.setState(new Closed());
    }
}
class Closed implements State {
    @Override
    public void open (DoorContext door) {
        System.out.println("Closed state");
        door.setState(new Opened());
    }
    @Override
    public void lock (DoorContext door) {
        System.out.println("Closed state");
        door.setState(new Locked());
    }
}
class Locked implements State {
    @Override
    public void unlock (DoorContext door) {
        System.out.println("Locked state");
        door.setState(new Closed());
    }
}


class DoorContext {
    private State state;
    
    public DoorContext() {
        state = new Opened();
    }

    void setState(State newState) {
        state = newState;
    }
}