
interface State {
       default void up (HeroContext context) {}
       default void down (HeroContext context) {}
       default void release (HeroContext context) {}
       default void defaultT (HeroContext context) {}
}


class Standing implements State {
    @Override
    public void up (HeroContext hero) {
        System.out.println("Standing state");
        hero.setState(new Jumping());
    }
    @Override
    public void down (HeroContext hero) {
        System.out.println("Standing state");
        hero.setState(new Crouching());
    }
}
class Jumping implements State {
    @Override
    public void down (HeroContext hero) {
        System.out.println("Jumping state");
        hero.setState(new Diving());
    }
    @Override
    public void defaultT (HeroContext hero) {
        System.out.println("Jumping state");
        hero.setState(new Standing());
    }
}
class Diving implements State {
    @Override
    public void defaultT (HeroContext hero) {
        System.out.println("Diving state");
        hero.setState(new Standing());
    }
}
class Crouching implements State {
    @Override
    public void release (HeroContext hero) {
        System.out.println("Crouching state");
        hero.setState(new Standing());
    }
}


class HeroContext {
    private State state;
    
    public HeroContext() {
        state = new Standing();
    }

    void setState(State newState) {
        state = newState;
    }
}